package Tarea1;


public class Polinomio {
    private Nodo head;

    public Polinomio() {// Orden O(1)
        head = null;
    }

    public void AddPolinomio(int m, int p) {//Orden (n)
        Nodo u = new Nodo(m,p,null);
        Nodo x, y;
        if(head == null){
            head = u;
            return;
        }else if(head.potenciaDeX<u.potenciaDeX){
            u.next = head;
            head = u;
            return;
        }
        x = head;
        y = x;
        while(x!=null && x.potenciaDeX>u.potenciaDeX){
            y=x;
            x=x.next;
        }
        if((x!=null)&&(x.potenciaDeX==u.potenciaDeX)){
            x.multiplo+=u.multiplo;
        }else{
            u.next = y.next;
            y.next=u;
        }
    }

    public String toString() { //Magnitud O(n)
        Nodo p = head.next;
        String messi = String.valueOf(head.multiplo) + "x^" + String.valueOf(head.potenciaDeX);
        while (p != null) {
            if (p.multiplo > 0) {
                if (p.potenciaDeX != 0) {
                    messi += "+" + p.multiplo + "x^" + p.potenciaDeX;
                } else {
                    messi += "+" + p.multiplo;
                }
            } else {
                if (p.multiplo < 0) {
                    if (p.potenciaDeX != 0) {
                        messi += p.multiplo + "x^" + p.potenciaDeX;
                    } else {
                        messi += p.multiplo;
                    }
                }
            }
            p = p.next;
        }
        return messi;
    }

    public Polinomio Suma(Polinomio a) {//Orden O(n^3)
        Nodo suma1 = head;
        Nodo suma2 = a.head;

        Polinomio sumado = new Polinomio();
        while(suma1!=null || suma2!=null) {
            if(suma1==null){
                while(suma2!=null){
                    sumado.AddPolinomio(suma2.multiplo,suma2.potenciaDeX);
                    suma2=suma2.next;
                }
                break;
            }else if(suma2==null){
                while(suma1!=null){
                    sumado.AddPolinomio(suma1.multiplo,suma1.potenciaDeX);
                    suma1=suma1.next;
                }
                break;
            }else if(suma1.potenciaDeX==suma2.potenciaDeX){
                sumado.AddPolinomio(suma1.multiplo+suma2.multiplo,suma1.potenciaDeX);
                suma1=suma1.next;
                suma2=suma2.next;
            }else if(suma1.potenciaDeX > suma2.potenciaDeX) {
                sumado.AddPolinomio(suma1.multiplo, suma1.potenciaDeX);
                suma1 = suma1.next;
            } else {
                sumado.AddPolinomio(suma2.multiplo, suma2.potenciaDeX);
                suma2 = suma2.next;
            }

        }
        return sumado;
    }

    public Polinomio multiplicacion(Polinomio polinomio){// Orden O(n^2)
        Nodo p=polinomio.head;
        Nodo q=head;
        Polinomio resultado=new Polinomio();


        //Multiplica termino a termino los polinomios y almacena los resultados en otro polinomio
        while(q!=null) {
            while(p!=null) {
                resultado.AddPolinomio(q.multiplo*p.multiplo,q.potenciaDeX+p.potenciaDeX);
                p=p.next;
            }
            q=q.next;
            p = polinomio.head;
        }

        Nodo x, y;
        x = resultado.head;

        /*Eliminar terminos semejantes sumandolos.
        while (x != null && x.next != null) {
            y = x;
            while (y.next != null) {
                if (x.potenciaDeX == y.next.potenciaDeX) {
                    x.multiplo = x.multiplo + y.next.multiplo;
                    y.next = y.next.next;
                }
                else
                    y = y.next;
            }
            x = x.next;
        }


         */
        return resultado;
    }

    public int evaluaPolinomio(int valor){//Orden O(n)
        Nodo p = head;
        int suma = 0;
        while(p!=null){
            suma+=p.multiplo*Math.pow(valor,p.potenciaDeX);
            p=p.next;
        }
        return suma;
    }

    public int gradoPolinomio(){ //O(1)
        return head.potenciaDeX;
    }


}

class Nodo {
    public int multiplo;
    public int potenciaDeX;
    public Nodo next;

    public Nodo(int multiplo, int potencia, Nodo next) {//Orden O(1)
        this.multiplo = multiplo;
        potenciaDeX = potencia;
        this.next = next;
    }

}