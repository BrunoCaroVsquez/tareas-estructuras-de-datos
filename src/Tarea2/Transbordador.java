package Tarea2;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;


class Transbordador {
    private Fila left = new Fila();
    private Fila right = new Fila();
    private Lista Vehiculos = new Lista();
    private Lista Tiempos = new Lista();
    private final int tamAutos;
    private final int momentoLlegada;

    public Transbordador(int Autos, int Tiempo) {//O(1)
        this.tamAutos = Autos;
        this.momentoLlegada = Tiempo;
    }

    public void addAuto(int llegada, String ubicacion) {//O(1)
        Vehiculo c = new Vehiculo(llegada);
        Vehiculos.add(c);
        if (ubicacion.equals("left")) {
            left.add(c);
        } else if (ubicacion.equals("right")) {
            right.add(c);
        }
    }

    public Lista simulador() {//O(n^2)
        int time = 0;
        Fila lado1 = left;
        Fila lado2 = right;
        while (!left.isEmpty() || !right.isEmpty()) {
            int carga = 0;
            while (carga < tamAutos && !lado1.isEmpty() && lado1.peek().minLlegada <= time) {
                Vehiculos.add(lado1.poll());
                Tiempos.add(Integer.toString(time + momentoLlegada));
                carga++;
            }

            if (carga > 0 || (!lado2.isEmpty() && lado2.peek().minLlegada <= time)) {
                time += momentoLlegada;
                Fila x = lado1;
                lado1 = lado2;
                lado2 = x;
            } else {
                Integer nexttime = null;
                if(!lado1.isEmpty()) {
                    nexttime = lado1.peek().minLlegada;
                }
                if(!lado2.isEmpty() && (nexttime == null || nexttime > lado2.peek().minLlegada)) {
                    nexttime = lado2.peek().minLlegada;
                }
                if(nexttime != null) {
                    time = nexttime;
                }
            }
        }

        return Tiempos;
    }

    private class Vehiculo {
        public int minLlegada;

        public Vehiculo(int min) {//O(1)
            this.minLlegada = min;
        }
    }

    class Fila{
        Nodo rear; Nodo front;
        Fila(){
            front=null;
            rear=null;
        }
        public void add(Object a){
            if(rear==null || front==null){
                rear = front = new Nodo(a);
            }else{
                rear.next = new Nodo(a);
                rear=rear.next;
            }
        }
        public Vehiculo poll(){
            if(isEmpty()){
                return null;
            }else{
                Vehiculo h = (Vehiculo) front.x;
                front = front.next;
                return h;
            }
        }
        public Vehiculo peek(){
            if(isEmpty()){
                return null;
            }else{
                Vehiculo h = (Vehiculo) front.x;
                return h;
            }
        }
        public boolean isEmpty(){
            return front ==null || rear==null;
        }
    }
    class Lista{
        Nodo head;
        Lista(){
            head=null;
        }
        public void add(Object e){
            if(isEmpty()){
                head=new Nodo(e);
            }else{
                Nodo p=head;
                while(p.next!=null){
                    p=p.next;
                }
                p.next=new Nodo(e);
            }
        }
        public boolean isEmpty(){
            return head==null;
        }
    }
    class Nodo{
        Object x;
        Nodo next;
        Nodo(Object a, Nodo x){
            this.x=a;
            next=x;
        }
        Nodo(Object a){
            x=a;
            next=null;
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Numero de casos:");
        int casos = in.nextInt();
        while (casos-- > 0) {
            System.out.println("Carga:");
            int carga = in.nextInt();
            System.out.println("Demora en minutos:");
            int demora = in.nextInt();
            System.out.println("Cantidad de autos");
            int cant = in.nextInt();
            Transbordador main = new Transbordador(carga, demora);
            while (cant-- > 0) {
                System.out.println("Tiempo de llegada de vehiculo:");
                int min = in.nextInt();
                System.out.println("Ubicacion del vehiculo:");
                String ubicacion = in.next();
                main.addAuto(min, ubicacion);
            }
            Lista sim = main.simulador();
            Nodo x = sim.head;
            while(x!=null){
                System.out.println(x.x);
                x=x.next;
            }


        }


    }
}